<?php

namespace Drupal\teamleader\Exception;

/**
 * Exception to throw when Teamleader credentials are invalid.
 */
class TeamleaderCredentialsException extends \Exception {}
