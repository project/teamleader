<?php

namespace Drupal\teamleader;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\State\StateInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\teamleader\Exception\TeamleaderCredentialsException;
use Teamleader\Client;
use Teamleader\Connection;

/**
 * Teamleader Integration service.
 *
 * @mixin \Teamleader\Client
 */
class TeamleaderApiService implements TeamleaderApiServiceInterface {

  /**
   * Teamleader connection instance.
   *
   * @var \Teamleader\Connection|null
   */
  protected $connection = NULL;

  /**
   * Teamleader client instance.
   *
   * @var \Teamleader\Client|null
   */
  protected ?Client $client = NULL;

  /**
   * The Key repository.
   *
   * @var \Drupal\key\KeyRepositoryInterface|null
   */
  protected ?KeyRepositoryInterface $keyRepository = NULL;

  /**
   * TeamleaderApiService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $urlGenerator
   *   The url generator.
   * @param \Drupal\Core\State\StateInterface $state
   *   The Drupal state api.
   */
  public function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly UrlGeneratorInterface $urlGenerator,
    protected readonly StateInterface $state,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function setKeyRepository(KeyRepositoryInterface $key_repository): void {
    $this->keyRepository = $key_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnection(): Connection {
    if (!$this->connection) {
      $settings = $this->configFactory->get('teamleader.settings');
      $credentials_provider = $settings->get('credentials.credentials_provider');

      // Get client ID and client secret from credentials provider.
      if ($credentials_provider === 'key' && $this->keyRepository) {
        $key_credentials = $this->keyRepository->getKey($settings->get('credentials.credentials_key'))->getKeyValues();
        if (!$key_credentials || !$key_credentials['client_id']) {
          throw new TeamleaderCredentialsException('Teamleader credentials are missing.');
        }
        $client_id = $key_credentials['client_id'];
        $client_secret = $key_credentials['client_secret'];
      }
      else {
        if (!$settings->get('credentials.client_id')) {
          throw new TeamleaderCredentialsException('Teamleader credentials are missing.');
        }
        $client_id = $settings->get('credentials.client_id');
        $client_secret = $settings->get('credentials.client_secret');
      }

      $cache_handler = new TeamleaderApiCacheHandler($this->state);
      $this->connection = new Connection($cache_handler);
      $this->connection->setClientId($client_id);
      $this->connection->setClientSecret($client_secret);
      $this->connection->setRedirectUrl($this->getRedirectUri());
    }

    return $this->connection;
  }

  /**
   * {@inheritdoc}
   */
  public function getClient(): Client {
    if (!$this->client) {
      $this->client = new Client($this->getConnection());
    }

    return $this->client;
  }

  /**
   * {@inheritdoc}
   */
  public function startAuthorization(): void {
    $connection = $this->getConnection();
    $connection->acquireAccessToken();
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectUri(): GeneratedUrl|string {
    return $this->urlGenerator->generateFromRoute('teamleader.settings_form', [], [
      'absolute' => TRUE,
    ]);
  }

  /**
   * Pass calls to the TL client.
   *
   * @param string $name
   *   TL client Method name.
   * @param mixed $arguments
   *   TL client Method arguments.
   *
   * @return \Teamleader\Model
   *   The TL model to use.
   *
   * @throws \Drupal\teamleader\Exception\TeamleaderCredentialsException
   * @throws \Exception
   */
  public function __call(string $name, $arguments) {
    $client = $this->getClient();

    if (method_exists($client, $name)) {
      return call_user_func_array([$client, $name], $arguments);
    }

    throw new \Exception(sprintf('Method %s not found', $name));
  }

}
