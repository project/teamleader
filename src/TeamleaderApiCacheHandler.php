<?php

namespace Drupal\teamleader;

use Drupal\Core\State\StateInterface;
use Teamleader\Handlers\CacheHandlerInterface;

/**
 * Teamleader api cache handler.
 *
 * A custom cache handler for the teamleader api
 * library that works with Drupal state api.
 *
 * @package Drupal\teamleader
 */
class TeamleaderApiCacheHandler implements CacheHandlerInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The Drupal State API.
   */
  public function __construct(protected readonly StateInterface $state) {}

  /**
   * Write value to store.
   *
   * @param string $key
   *   The store key.
   * @param mixed $value
   *   The store value.
   * @param int $expireInMinutes
   *   The store expiration in minutes.
   */
  public function set($key, $value, $expireInMinutes) {
    $data = [
      'value' => $value,
      'expires' => time() + ($expireInMinutes * 60),
    ];

    $this->state->set($key, $data);
  }

  /**
   * Get value from store.
   *
   * @param mixed $key
   *   The store key.
   *
   * @return mixed
   *   The store value.
   */
  public function get($key) {
    $data = $this->state->get($key);

    if (isset($data['expires']) && $data['expires'] < time()) {
      $this->forget($key);

      return NULL;
    }

    return $data['value'] ?? NULL;
  }

  /**
   * Remove from store.
   *
   * @param mixed $key
   *   The store key.
   */
  public function forget($key) {
    $this->state->delete($key);
  }

}
