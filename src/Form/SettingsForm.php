<?php

namespace Drupal\teamleader\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Utility\Error;
use Drupal\key\KeyRepository;
use Drupal\teamleader\Exception\TeamleaderCredentialsException;
use Drupal\teamleader\TeamleaderApiServiceInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Teamleader settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  use MessengerTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'teamleader_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['teamleader.settings'];
  }

  /**
   * TeamleaderSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The teamleader logger channel.
   * @param \Drupal\teamleader\TeamleaderApiServiceInterface $teamleaderApi
   *   The Teamleader API service.
   * @param \Drupal\key\KeyRepository|null $keyRepository
   *   The key repository service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    protected LoggerChannelInterface $logger,
    protected TeamleaderApiServiceInterface $teamleaderApi,
    protected ?KeyRepository $keyRepository,
  ) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $module_handler */
    $module_handler = $container->get('module_handler');
    $keyRepository = NULL;
    if ($module_handler->moduleExists('key')) {
      $keyRepository = $container->get('key.repository');
    }
    return new static(
      $container->get('config.factory'),
      $container->get('logger.channel.teamleader'),
      $container->get('teamleader_api'),
      $keyRepository
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): TrustedRedirectResponse|array {
    $form = parent::buildForm($form, $form_state);

    $connection = NULL;
    $connected = FALSE;
    try {
      $connection = $this->teamleaderApi->getConnection();
    }
    catch (TeamleaderCredentialsException $e) {
    }

    // Handle coming back from TL after approval.
    if ($this->getRequest()->query->get('code')) {
      try {
        $this->teamleaderApi->startAuthorization();
        return new TrustedRedirectResponse($this->teamleaderApi->getRedirectUri());
      }
      catch (\Exception $e) {
        $this->messenger()->addError($e->getMessage());
        Error::logException($this->logger, $e);
      }
    }

    try {
      if ($connection && $connection->getAccessToken()) {
        $this->messenger()->addStatus($this->t('Access token is present. Teamleader is connected'));
        $connected = TRUE;
      }
    }
    catch (ClientException $e) {
      $this->messenger()->addError($this->t('A previous token was present, but it looks like something went wrong (maybe it was revoked?). Please reconnect.'));
      Error::logException($this->logger, $e);
    }

    $form['actions']['submit']['#value'] = $this->t('Connect to Teamleader');

    $form['credentials_provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Credentials provider'),
      '#options' => [
        'config' => $this->t('Local configuration'),
      ],
      '#default_value' => $this->config('teamleader.settings')->get('credentials.credentials_provider'),
    ];

    if ($this->keyRepository !== NULL) {
      $form['credentials_provider']['#options']['key'] = $this->t('Key');
    }

    $form['credentials'] = [
      '#type' => 'details',
      '#title' => $this->t('Credentials'),
      '#open' => TRUE,
      '#description' => $this->t(
        'To retrieve your client ID & secret: <ol>
          <li>Visit <a href="@url">Teamleader Marketplace</a> and create a new integration.</li>
          <li>Retrieve your client ID/Secret under the "OAuth2 credentials" header.</li>
          </ol>',
        [
          '@url' => 'https://marketplace.teamleader.eu/be/en/build',
        ]
      ),
    ];

    $form['credentials']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#disabled' => $connected ?: FALSE,
      '#default_value' => $this->config('teamleader.settings')->get('credentials.client_id'),
      '#size' => 100,
      '#maxlength' => 150,
      '#description' => $this->t('The Teamleader integration client ID.'),
      '#states' => [
        'visible' => [
          ':input[name="credentials_provider"]' => ['value' => 'config'],
        ],
        'required' => [
          ':input[name="credentials_provider"]' => ['value' => 'config'],
        ],
      ],
    ];

    $form['credentials']['client_secret'] = [
      '#type' => 'textfield',
      '#disabled' => $connected ?: FALSE,
      '#title' => $this->t('Client secret'),
      '#default_value' => $this->config('teamleader.settings')->get('credentials.client_secret'),
      '#description' => $this->t('The Teamleader integration Client secret.'),
      '#states' => [
        'visible' => [
          ':input[name="credentials_provider"]' => ['value' => 'config'],
        ],
        'required' => [
          ':input[name="credentials_provider"]' => ['value' => 'config'],
        ],
      ],
    ];

    if ($this->keyRepository !== NULL) {
      $form['credentials']['credentials_key'] = [
        '#type' => 'key_select',
        '#title' => $this->t('Key'),
        '#description' => $this->t('The key should contain the OAuth credentials for Teamleader. Choose key type "Authentication (Multivalue)", and add the properties "client_id" and "client_secret" to the key value.')
        . $this->t('<br />Example key data: <code>{"client_id": "47012495342576573474", "client_secret": "51106350561134283737"}</code>'),
        '#default_value' => $this->config('teamleader.settings')->get('credentials.credentials_key'),
        '#empty_option' => $this->t('- Please select -'),
        '#key_filters' => ['type' => 'authentication_multivalue'],
        '#disabled' => $connected ?: FALSE,
        '#states' => [
          'visible' => [
            ':input[name="credentials_provider"]' => ['value' => 'key'],
          ],
          'required' => [
            ':input[name="credentials_provider"]' => ['value' => 'key'],
          ],
        ],
      ];
    }

    if ($connected) {
      $form['actions']['submit']['#access'] = FALSE;
      $form['actions']['disconnect'] = [
        '#type' => 'submit',
        '#value' => 'Disconnect',
        '#submit' => ['::clearAccessTokens'],
      ];
    }

    return $form;
  }

  /**
   * Clears the current TL access tokens.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function clearAccessTokens(array &$form, FormStateInterface $form_state): void {
    $this->teamleaderApi->getConnection()->clearTokens();
    $this->messenger()->addMessage($this->t('Tokens successfully cleared'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $credentials_provider = $form_state->getValue('credentials_provider');
    $this->config('teamleader.settings')
      ->set('credentials.credentials_provider', $credentials_provider);
    if ($credentials_provider === 'config') {
      $this->config('teamleader.settings')
        ->set('credentials.client_id', $form_state->getValue('client_id'))
        ->set('credentials.client_secret', $form_state->getValue('client_secret'))
        ->set('credentials.credentials_key', '');
    }
    elseif ($credentials_provider === 'key') {
      $this->config('teamleader.settings')
        ->set('credentials.client_id', '')
        ->set('credentials.client_secret', '')
        ->set('credentials.credentials_key', $form_state->getValue('credentials_key'));
    }
    $this->config('teamleader.settings')->save();

    try {
      $this->teamleaderApi->startAuthorization();
    }
    catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
    }
  }

}
