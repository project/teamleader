<?php

namespace Drupal\teamleader;

use Drupal\Core\GeneratedUrl;
use Drupal\key\KeyRepositoryInterface;
use Teamleader\Client;
use Teamleader\Connection;

/**
 * Interface for Teamleader API service.
 *
 * @mixin \Teamleader\Client
 */
interface TeamleaderApiServiceInterface {

  /**
   * Set optional key repository service.
   *
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   The key repository service.
   */
  public function setKeyRepository(KeyRepositoryInterface $key_repository): void;

  /**
   * Returns the teamleader connection instance.
   *
   * @return \Teamleader\Connection
   *   A configured teamleader connection instance.
   */
  public function getConnection(): Connection;

  /**
   * Returns the Teamleader client.
   *
   * @return \Teamleader\Client
   *   The teamleader client.
   *
   * @throws \Drupal\teamleader\Exception\TeamleaderCredentialsException
   */
  public function getClient(): Client;

  /**
   * Attempt to get an access token.
   *
   * @throws \Teamleader\Exceptions\ApiException
   * @throws \Drupal\teamleader\Exception\TeamleaderCredentialsException
   */
  public function startAuthorization(): void;

  /**
   * Return the redirection uri.
   *
   * @return string|\Drupal\Core\GeneratedUrl
   *   The redirect uri.
   */
  public function getRedirectUri(): GeneratedUrl|string;

}
