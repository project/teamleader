<?php

namespace Drupal\teamleader_contact;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\teamleader\TeamleaderApiServiceInterface;

/**
 * Service to integrate Teamleader with Drupal core Contact module.
 */
class TeamleaderContact implements TeamleaderContactInterface {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * Constructs a TeamleaderContact class.
   *
   * @param \Drupal\teamleader\TeamleaderApiServiceInterface $teamleader
   *   The Teamleader API service.
   */
  public function __construct(
    protected readonly TeamleaderApiServiceInterface $teamleader,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function addContactToTeamleader(array $form, FormStateInterface &$form_state): void {
    try {
      $email = $form_state->getValue('mail');
      $name = explode(" ", $form_state->getValue('name'));
      $first_name = array_shift($name);
      $last_name = implode(" ", $name);

      $email_data = [
        'type' => 'primary',
        'email' => $email,
      ];
      $contact = $this->teamleader->contact([
        'first_name' => $first_name,
        'last_name' => $last_name,
        'emails' => [(object) $email_data],
      ]);

      $contact->save();
    }
    catch (\Exception $ex) {
      $this->messenger()->addError($this->t('Error adding contact to Teamleader: "%error".', ['%error' => $ex->getMessage()]));
    }
  }

}
