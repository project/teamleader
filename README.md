# Description

This module integrates your Drupal website with Teamleader
(https://www.teamleader.eu/).

It connects your site to the v2 API of Teamleader.

# Installation

* Install the Drupal module via Composer:
  `composer require drupal/teamleader --update-with-dependencies`
* Go to /admin/config/services/teamleader and follow instructions to enter
  your Teamleader app client ID & secret.

# Usage

To interact with the Teamleader API in your custom module, use the
`teamleader_api` service, and call the `getClient()` method on it:

```
/** @var \Teamleader\Client $teamleader */
$teamleader = \Drupal::service('teamleader_api');
```

Please note that you should preferably use dependency injection to load the
service, instead of `\Drupal::service()`.

Now that you have access to the teamleader client, you can interact with the
Teamleader data objects, e.g. get a list of contacts:

```
$contacts = $teamleader->contact()->get();
```

See the teamleader [library documentation](https://github.com/janhenkes/teamleader-php-sdk/tree/master/examples) that is used in this module for
more information about all available methods and how to use them.
